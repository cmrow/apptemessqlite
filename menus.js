/**
 * 
 */

var menus = [
    {
        subtemas:
            [
                {
                    nombreSubtema: "Web semantic",
                    url: "pages/html/basic.html"
                },
                {
                    nombreSubtema: "SEO",
                    url: "pages/html/seo.html"
                },
            ]
    },
    {
        subtemas:
            [
                {
                    nombreSubtema: "Selectores",
                    url: "pages/html/selectores.html"
                },
                {
                    nombreSubtema: "FlexBox",
                    url: "pages/html/flexbox.html"
                },
            ]
    }
]