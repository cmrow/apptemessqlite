myApp.define('services', ['dataBase/main'], (dataBase) => {

    const teme = idUser => row => ({
        id: row['id'],
        teme: row['teme'],
        description: row['description'],
        idPatern: row['idPatern'],
        idCreador: row['idCreador'] == idUser
    });

    return {
        registerUser: (user, fnNext) => dataBase.insert('INSERT INTO users(name, lastName, email, password) VALUES (?,?,?,?)', [
            user.name,
            user.lastName,
            user.email,
            user.password
        ], fnNext),
        //
        //INSERT INTO temes(teme, description, idPatern, idCreator) VALUES (?,?,?,?)
        registerTeme: (tem, fnNext) => dataBase.insert('INSERT INTO temes(description)value("jj")', [
            tem.teme,
            tem.description,
            tem.idPatern,
            tem.idCreator
        ], fnNext),
        getTemes: (idUser, fnNext) => dataBase.select(`SELECT * FROM temes WHERE idCreator = ${idUser}`, teme(idUser), fnNext),
    }
});
