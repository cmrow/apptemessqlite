myApp.define('dataBase/main', [], () => {
    const sqls = [
        //CREATE TABLES
        'CREATE TABLE IF NOT EXISTS users(id integer primary key autoincrement, name text, lastName text, email text unique, password text)',
        'CREATE TABLE IF NOT EXISTS temes(id integer primary key autoincrement, teme text, description text, idPatern text, idCreator integer, FOREIGN KEY (idCreator) REFERENCES users(id) )',
        //INSERT users
        `INSERT INTO users(name, lastName, email, password) SELECT "Semillero", "GTH", "semillero@sinco.com.co", "admin123" WHERE NOT EXISTS (SELECT 1 FROM users WHERE email = "semillero@sinco.com.co")`,
        //INSERT temes
        'INSERT INTO temes(teme , description , idPatern, idCreator ) SELECT "HTML (Hyper Text Markup Languaje)", "Es un lenguaje que nos permite definir la estructura de una página web.", "0", 1 WHERE NOT EXISTS (SELECT 1 FROM temes WHERE id = 1)'

    ];

    const createDbConnection = () => openDatabase('temary_db', '1.0', 'RoadMap Temes list', 50 * 1024 * 1024);

    const initDatabase = () => {
        const db = createDbConnection();
        // debugger
        db.transaction(tx => {
            const ex = sql => tx.executeSql(sql, [], (tx, result) => {
                console.log(`Migration result : ${result}\nQuery executed : ${sql}`);
                let query = sqls.shift();
                if (query) ex(query)

            });
            ex(sqls.shift());
        });
    };
    initDatabase();

    // const initTemes = () => {
    //     let sqlTemesTable = 'CREATE TABLE IF NOT EXISTS temes(id integer primary key autoincrement, teme text, description text, idPatern text, idCreator integer, FOREIGN KEY (idCreator) REFERENCES users(id) )';
    //     let sqlTemesInsert = 'INSERT INTO temes(teme , description , idPatern, idCreator ) SELECT "HTML (Hyper Text Markup Languaje)", "Es un lenguaje que nos permite definir la estructura de una página web.", "0", 1 WHERE NOT EXISTS (SELECT 1 FROM temes WHERE id = 1)';

    //     db.transaction(tx => {
    //         tx.executeSql(sqlTemesTable);
    //         tx.executeSql(sqlTemesInsert);
    //     });
    // };
    // initTemes();

    const select = (sql, map, next) => {
        db.transaction(tx => tx.executeSql(sql, [], (tx, result) => {
            const values = [].map.call(result.rows, map);
            next(values);
        }));
    };

    const insert = (sql, data, next) => {
        db.transaction(tx => tx.executeSql(sql, data, (tx, results) => {
            next(results);
        }));
    }

    const salectFirst = (sql, map, next) => select(sql, map, values => next(values.shift()));

    return {
        select,
        insert,
        salectFirst
    };
});
/**
 * LINKS :
 * https://programacion.net/articulo/introduccion_a_web_sql_1305
 * https://www.arkaitzgarro.com/html5/capitulo-8.html
 * https://www.w3.org/TR/webdatabase/#sql
 */
